Hooks:PostHook(SkillTreeTweakData,"init","tmasq_initskilltree",function(self,data)
	
	-- the convention for internal perk deck name/desc id naming is more like:
	-- "menu_deck#_title"
	-- where # is replaced by a number. for instance, the name_id for "Crew Chief" is "menu_deck1_title".
	-- this means that this naming convention (eg "menu_deck_tmasq_nosferatu_title") is actually unlikely to produce conflicts with payday 2's code,
	-- though it may conflict with other perk deck mods if you don't give your mod a unique enough name. 
	
	--category: current valid categories are:
	--"offensive"
	--"defensive"
	--"covert"
	--"supportive"
	--"versatile"
	--"stealth"
	
	--the category field can either be one of these, or a table containing any number of these categories.
	
	local perk_deck_data = {
		name_id = "menu_deck_tmasq_nosferatu_title",
		desc_id = "menu_deck_tmasq_nosferatu_desc",
		category = {
			"mod"
		},
		{
			name_id = "menu_deck_tmasq_nosferatu_1",
			desc_id = "menu_deck_tmasq_nosferatu_1_desc",
			cost = 0,
			upgrades = {
				"player_tmasq_kindred",
				"player_tmasq_potence1",
				"player_tmasq_obfuscate1",
--				"player_passive_health_multiplier_1"
			},
			texture_bundle_folder = "tmasq",
			icon_xy = {
				0,
				0
			}
		},
		{
			name_id = "menu_deckall_2",
			desc_id = "menu_deckall_fixed_2_desc",
			cost = 0,
			upgrades = {
				-- "weapon_passive_headshot_damage_multiplier"
			},
			icon_xy = {1,0}
		},
		{
			name_id = "menu_deck_tmasq_nosferatu_3",
			desc_id = "menu_deck_tmasq_nosferatu_3_desc",
			cost = 0,
			upgrades = {
				-- "player_tmasq_nosferatu_reload_speed_bonus_per_max_armor",
				-- "player_tmasq_nosferatu_swap_speed_bonus_per_max_armor",
				-- --"player_tmasq_nosferatu_crit_chance_per_max_armor",
				-- "player_tier_armor_multiplier_1"
			},
			texture_bundle_folder = "tmasq",
			icon_xy = {
				1,
				0
			}
		},
		{
			name_id = "menu_deckall_4",
			desc_id = "menu_deckall_fixed_4_desc",
			cost = 0,
			upgrades = {
				-- "passive_player_xp_multiplier",
				-- "player_passive_suspicion_bonus",
				-- "player_passive_armor_movement_penalty_multiplier"
			},
			icon_xy = {3,0}
		},
		{
			name_id = "menu_deck_tmasq_nosferatu_5",
			desc_id = "menu_deck_tmasq_nosferatu_5_desc",
			cost = 0,
			upgrades = {
-- 				"player_tmasq_nosferatu_fatal_triggers_invuln",
-- 				"player_tmasq_nosferatu_passive_health_multiplier_2"
-- --				"player_passive_health_multiplier_2"
			},
			texture_bundle_folder = "tmasq",
			icon_xy = {
				2,
				0
			}
		},
		{
			name_id = "menu_deckall_6",
			desc_id = "menu_deckall_fixed_6_desc",
			cost = 0,
			upgrades = {
				-- "armor_kit",
				-- "player_pick_up_ammo_multiplier"
			},
			icon_xy = {5,0}
		},
		{
			name_id = "menu_deck_tmasq_nosferatu_7",
			desc_id = "menu_deck_tmasq_nosferatu_7_desc",
			cost = 0,
			upgrades = {
				-- "player_tmasq_nosferatu_armored_hot",
				-- "player_tier_armor_multiplier_2",
				-- "player_tier_armor_multiplier_3"
			},
			texture_bundle_folder = "tmasq",
			icon_xy = {
				3,
				0
			}
		},
		{
			name_id = "menu_deckall_8",
			desc_id = "menu_deckall_fixed_8_desc",
			cost = 0,
			upgrades = {
				-- "weapon_passive_damage_multiplier",
				-- "passive_doctor_bag_interaction_speed_multiplier"
			},
			icon_xy = {7,0}
		},
		{
			name_id = "menu_deck_tmasq_nosferatu_9",
			desc_id = "menu_deck_tmasq_nosferatu_9_desc",
			cost = 0,
			upgrades = {
				-- "player_tmasq_nosferatu_bloody_armor"
			},
			texture_bundle_folder = "tmasq",
			icon_xy = {
				0,
				1
			}
		}
	}
	
	--the specializations table is where all perk decks are stored; we will insert our perk deck data here so that the game can add it and we can equip it
	table.insert(self.specializations,perk_deck_data)
	
	Hooks:Add("LocalizationManagerPostInit", "themasq_addlocalization", function( loc )

-- 	--due to how overkill has set up localization macros (selectively replacing certain substrings in a localization string),
-- 	--we can't just copy the existing localization ids for the descriptions (the names are fine).
	
-- --		the 'generic' perkdeck cards ("menu_deckall_2_desc" etc) have a macro localization issue when referenced directly
-- --		so we need to fix it for our deck here and substitute the macro keys with the proper values

		
-- 		local adjusted_description_2 = loc:text("menu_deckall_2_desc")
-- 		adjusted_description_2 = string.gsub(adjusted_description_2,"$multiperk;","25%%")
-- 		local adjusted_description_4 = loc:text("menu_deckall_4_desc")
-- 		adjusted_description_4 = string.gsub(adjusted_description_4,"$multiperk;","+1")
-- 		adjusted_description_4 = string.gsub(adjusted_description_4,"$multiperk2;","15%%")
-- 		adjusted_description_4 = string.gsub(adjusted_description_4,"$multiperk3;","45%%")
-- 		local adjusted_description_6 = loc:text("menu_deckall_6_desc")
-- 		adjusted_description_6 = string.gsub(adjusted_description_6,"$multiperk;","135%%")
-- 		local adjusted_description_8 = loc:text("menu_deckall_8_desc")
-- 		adjusted_description_8 = string.gsub(adjusted_description_8,"$multiperk;","5%%")
-- 		adjusted_description_8 = string.gsub(adjusted_description_8,"$multiperk2;","20%%")
		
-- 		loc:add_localized_strings({
-- 			menu_deckall_fixed_2_desc = adjusted_description_2,
-- 			menu_deckall_fixed_4_desc = adjusted_description_4,
-- 			menu_deckall_fixed_6_desc = adjusted_description_6,
-- 			menu_deckall_fixed_8_desc = adjusted_description_8 
-- 		})
	end)
	
end)