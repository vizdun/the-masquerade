tmasq_masq_viols = {}

local function themasq_unit_id(unit)
    return tostring(unit):gmatch("([0-9A-F]+)]")()
end

local function masq_viols()
    local count = 0
    for _, value in pairs(tmasq_masq_viols) do
        if value then
            count = count + 1
        end
    end
    return count
end

-- functions i tried before i found this one:
--
-- CivilianLogicIdle:die
-- CopBrain:on_alert
-- CopLogicIdle.on_alert
-- CopLogicBase.on_alert
-- CopLogicIntimidated.on_alert
-- CopBrain:on_detected_enemy_destroyed
-- CopBrain:on_detected_attention_obj_modified
-- CopBrain:on_detected_attention_obj_tweak_data_changed
-- CopLogicBase._upd_suspicion
-- CopLogicBase.on_attention_obj_identified
-- GroupAIStateBase:on_unit_detection_updated
-- GroupAIStateBase:_upd_criminal_suspicion_progress
Hooks:PreHook(GroupAIStateBase, "on_criminal_suspicion_progress", "tmasq_on_criminal_suspicion_progress", function (self, u_suspect, u_observer, status)
    if status == true then
        tmasq_masq_viols[themasq_unit_id(u_observer)] = true
        log(masq_viols())
    end
end)
