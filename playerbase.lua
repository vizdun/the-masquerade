Hooks:PostHook(PlayerBase, "update_concealment", "tmasq_update_concealment", function (self)
    if managers.player:has_category_upgrade("player","tmasq_obfuscate1") then
        self:set_detection_multiplier("tmasq_obfuscate", 0.75)
    end
end)
