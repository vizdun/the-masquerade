Hooks:PostHook(UpgradesTweakData,"_player_definitions","tmasq_player_definitions",function(self)


	self.values.player.tmasq_kindred = {
        
	}

	self.values.player.tmasq_potence1 = {
        
	}

	self.values.player.tmasq_obfuscate1 = {
        
	}
	
	self.definitions.player_tmasq_kindred = {
		name_id = "menu_deck_ghost_vinight_1_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "tmasq_kindred",
			category = "player"
		}
	}

    self.definitions.player_tmasq_potence1 = {
		name_id = "menu_deck_ghost_vinight_1_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "tmasq_potence1",
			category = "player"
		}
	}

    self.definitions.player_tmasq_obfuscate1 = {
		name_id = "menu_deck_ghost_vinight_1_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "tmasq_obfuscate1",
			category = "player"
		}
	}
	-- self.definitions.ghost_skill_2 = {
	-- 	name_id = "menu_deck_ghost_vinight_3_title",
	-- 	category = "feature",
	-- 	upgrade = {
	-- 		value = 1,
	-- 		upgrade = "ghost_haste",
	-- 		category = "player"
	-- 	}
	-- }
	-- self.definitions.ghost_skill_3 = {
	-- 	name_id = "menu_deck_ghost_vinight_5_title",
	-- 	category = "feature",
	-- 	upgrade = {
	-- 		value = 1,
	-- 		upgrade = "destroy_faith",
	-- 		category = "player"
	-- 	}
	-- }
	-- self.definitions.ghost_skill_4 = {
	-- 	name_id = "menu_deck_ghost_vinight_7_title",
	-- 	category = "feature",
	-- 	upgrade = {
	-- 		value = 1,
	-- 		upgrade = "ghost_save",
	-- 		category = "player"
	-- 	}
	-- }
	-- self.definitions.ghost_skill_5 = {
	-- 	name_id = "menu_deck_ghost_vinight_9_title",
	-- 	category = "feature",
	-- 	upgrade = {
	-- 		value = 1,
	-- 		upgrade = "ghost_galloped",
	-- 		category = "player"
	-- 	}
	-- }
end)