local function themasq_unit_id(unit)
    return tostring(unit):gmatch("([0-9A-F]+)]")()
end

local function masq_viols()
    local count = 0
    for _, value in pairs(tmasq_masq_viols) do
        if value then
            count = count + 1
        end
    end
    return count
end

Hooks:PostHook(BossDamage, "die", "tmasq_drain_boss", function (self, attack_data)
    tmasq_masq_viols[themasq_unit_id(self._unit)] = false
    log(masq_viols())

    if attack_data.variant == "melee" then
        log("take a boss shot")
    end

    return Hooks:GetReturn()
end)