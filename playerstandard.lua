Hooks:PreHook(PlayerStandard, "_start_action_jump", "tmasq_start_action_jump", function (self, t, action_start_data)
    if managers.player:has_category_upgrade("player","tmasq_potence1") then
        action_start_data.jump_vel_z = action_start_data.jump_vel_z * 2

        action_start_data.jump_vel_xy = (action_start_data.jump_vel_xy or 0) * 2
    end
end)