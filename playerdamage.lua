local daylightMaps = {
    jewelry_store = true,
    branchbank = true,
    branchbank_deposit = true,
    branchbank_cash = true,
    branchbank_prof = true,
    branchbank_gold_prof = true,
    cage = true,
    roberts = true,
    arm_cro = true,
    arm_hcm = true,
    arm_fac = true,
    arm_par = true,
    arm_und = true,
    arm_for = true,
    red2 = true,
    run = true,
    flat = true,
    dinner = true,
    pal = true,
    man = true,
    wwh = true,
    brb = true,
    bph = true,
    des = true,
    pbr = true,
    Berry = true,
    pbr2 = true,
    Jerry = true,
    mex = true,
    pex = true,
    crojob1 = true,
    crojob2 = true,
    crojob3 = true,
    thebomb_stage_2 = true,
    thebomb_stage_3  = true,
    friend = true,
    big = true,
    mus = true,
    hox = true,
    hox_1 = true,
    hox_2 = true,
    welcome_to_the_jungle = true,
    election_day = true,
    born = true,
    chew = true,
    jolly = true,
    holly_2 = true,
    four_stores = true,
    mallcrasher = true,
    shoutout_raid = true,
    cane = true,
    ukrainian_job = true,
    pines = true,
    san = true,
    peta = true,
    bex = true,
    fex = true,
    ranc = true,
    mad = true,
    chill_combat = true,
}

local cached = nil
local function shouldDie()
    if cached == nil then
        cached = daylightMaps[managers.job:current_job_id()] and managers.player:has_category_upgrade("player","tmasq_kindred")
    end
    return cached
end

Hooks:PreHook(PlayerDamage, "update", "tmasq_update", function (self, unit, t, dt)
    if shouldDie() then
        self:force_into_bleedout()
    end
end)